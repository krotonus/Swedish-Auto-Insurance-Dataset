# -*- coding: utf-8 -*-
"""
Created on Sat Oct 21 03:59:15 2017

@author: HOME
"""

import numpy as np
import pandas as pd

import matplotlib.pyplot as plt
import seaborn as sns
import missingno

from sklearn import preprocessing
from sklearn import linear_model
from sklearn import metrics

data_df = pd.read_csv("https://www.math.muni.cz/~kolacek/docs/frvs/M7222/data/AutoInsurSweden.txt", sep = '\t', skiprows = 10, header = 0)
#The Y column has two features, amount of money paid along with the geographical location in Sweden
# Bullshit. Its correct only. I was wrong. But i still have to replace commas with dots
'''
test_df = data_df['Y'].apply(lambda x: pd.Series(x.split(',')))
data_df['Y'] = test_df[0]
data_df['location'] = test_df[1]
'''
data_df['Y'] = data_df['Y'].apply(lambda x: pd.Series( x.replace(',','.')))
data_df.Y = data_df.Y.astype('float64')

#Visualisation
plt.figure( figsize= (15,5))
ax = plt.subplot(1,2,1)
#ax.set_title("X")
sns.distplot(data_df.X, bins = 50)
ax = plt.subplot(1,2,2)
#ax.set_title("Y")
sns.distplot(data_df.Y, bins = 50)

missingno.matrix(data_df)
data_df.apply(lambda x : x.isnull().sum()) # Number of missing location values in each column

#Note to self 
#https://github.com/RishiSD/Linear-regression-on-Swedish-Auto-Insurance-dataset/blob/master/Swedish%20Auto%20Insurance.ipynb

scaler = preprocessing.StandardScaler().fit(data_df.X.values.reshape(-1, 1).astype('float64'))
X_scaled = scaler.transform(data_df.X.values.reshape(-1, 1).astype('float64'))
Y_scaled = scaler.transform(data_df.Y.values.reshape(-1, 1).astype('float64'))

model = linear_model.LinearRegression()
model.fit(data_df.X.values.reshape(-1, 1), data_df.Y.values.reshape(-1, 1))

Y_pred = model.predict(data_df.X.values.reshape(-1, 1))
mse = metrics.mean_squared_error(data_df.Y.values.reshape(-1, 1), Y_pred)
print(np.sqrt(mse))
