import numpy as np
import pandas as pd

import matplotlib.pyplot as plt
import seaborn as sns
import missingno

from sklearn import preprocessing
from sklearn import linear_model
from sklearn import metrics
from sklearn.cross_validation import cross_val_score, train_test_split, KFold
from sklearn.model_selection import learning_curve

data_df = pd.read_csv("https://www.math.muni.cz/~kolacek/docs/frvs/M7222/data/AutoInsurSweden.txt", sep = '\t', skiprows = 10, header = 0)

data_df['Y'] = data_df['Y'].apply(lambda x: pd.Series( x.replace(',','.')))
data_df.X = data_df.X.astype('float64')
data_df.Y = data_df.Y.astype('float64')

plt.figure(figsize=(20,5))
plt.subplot(1,2,1)
sns.distplot(data_df.X, bins = 50)
plt.subplot(1,2,2)
sns.distplot(data_df.Y, bins = 50)
#Both the plots have similar shape.

plt.figure(figsize= (20,5))
sns.regplot(x = data_df.X, y =data_df.Y, data = data_df)

X = data_df.X.values.reshape(-1, 1)
y = data_df.Y.values.reshape(-1, 1)

regr = linear_model.LinearRegression()
regr.fit(X,y)
Y_pred = regr.predict(X)
mse = metrics.mean_squared_error(y, Y_pred)

regr_cv = linear_model.LinearRegression()
scores = cross_val_score(regr_cv, X, y, cv = 10, scoring = 'mean_squared_error')
scores = scores * (-1)

print('MSE = ' + str(np.mean(np.sqrt(scores))))

X_train, X_test, Y_train, Y_test = train_test_split(X, y, test_size = 0.3, random_state = 42)

plt.figure(figsize = (20,5))
sns.regplot(X_train, Y_train, fit_reg = False)

regr_fin = linear_model.LinearRegression()
regr_fin.fit(X_train, Y_train)
Y_pred = regr_fin.predict(X_test)
score = np.sqrt(metrics.mean_squared_error(Y_test, Y_pred))

print('MSE = ', score)